function filterData1(data1) {
    // variabel untuk menampung hasil
    const result = [];
    // variabel untuk menampung object
    let dataValue = 0
    // variabel untuk menyimpan apabila data tidak ditemukan
    let empty = 0

    for(let i = 0; i<data1.length; i++){
        if(data1[i].age < 30 && data1[i].favoriteFruit === "banana"){
            result[dataValue] = data1[i]
            dataValue++
        }
        // if(!result.length){
        //     notFound = "message : data tidak ada/tidak di temukan"
        //     result[empty] = notFound
        // }
    }
    return result 
}

const data = require("../data.js")
// console.log(filterData1(data))
module.exports = filterData1(data)

// age nya dibawah 30 tahun dan favorit buah nya pisang