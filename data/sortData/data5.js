function filterData5(data5) {
    // variabel untuk menampung hasil
    const result = [];
    // variabel untuk menampung aobject
    let dataValue = 0
    // variabel untuk menyimpan apabila data tidak ditemukan
    let empty = 0

    for(let i = 0; i<data5.length; i++){
        let date = "2016-01-01 T 00:00:00 - 00:00"
        if(data5[i].registered < date && data5[i].isActive === true){
            result[dataValue] = data5[i]
            dataValue++
        }
        // if(!result.length){
        //     const notFound = {message : "data tidak ada/tidak di temukan"}
        //     return
        // }
    }
    return result 
}

const data = require("../data.js")
console.log(filterData5(data))
module.exports = filterData5(data)


// registered di bawah tahun 2016 dan masih active(true)