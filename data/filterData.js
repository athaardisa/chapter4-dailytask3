module.exports = {
    filterData : function(allData, attribute){
        if(attribute == undefined || attribute == ""){
            return allData
        }
        // let result = []
        let result = allData.filter((data)=> {
            const attr = (attribute == "") ? attribute : attribute.toLowerCase()
            return data.favoriteFruit === attr;
        })
        return result
    }
}